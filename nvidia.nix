  services.xserver.videoDrivers = ["nvidia" ];
  hardware.nvidia.optimus_prime.enable = true;
  hardware.nvidia.optimus_prime.nvidiaBusId = "PCI:1:0:0";
  hardware.nvidia.optimus_prime.intelBusId = "PCI:0:2:0";
  hardware.nvidia.modesetting.enable = true;
  hardware.opengl.driSupport32Bit = true;
